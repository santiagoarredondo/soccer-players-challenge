def populate_db(db_connection):

	load_script = open('static/insert_csv.sql')

	try:
		with db_connection.cursor() as cursor:
			cursor.execute(load_script.read())
			print('\nDatabase successfully populated.')
	except Error:
		print('\nAn unexpected error has occurred.')
	finally:
		cursor.close()
		db_connection.commit()
		db_connection.close()